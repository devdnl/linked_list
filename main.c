#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "llist.h"

#define ASSERT(expr, ...) if (!(expr)) {printf("%s:%d: ", __FILE__, __LINE__); printf(__VA_ARGS__); puts(""); abort();}
#define TEST_NAME() printf("%s: ", __func__);
#define TEST_SUCCESS() printf("SUCCESS\n");
#define array_count(a) ((int)(sizeof(a)/sizeof(a[0])))

const char *cs[] = {
        "Test string 0\n",
        "Test string 1\n",
        "Test string 2\n",
        "Test string 3\n",
        "Test string 4\n",
        "Test string 5\n",
        "Test string 6\n",
        "Test string 7\n",
        "Test string 8\n",
        "Test string 9\n",
};

char *ds[10] = {NULL};

static char *string_new(const char *s)
{
        char *mem = calloc(1, strlen(s));
        if (mem) {
                strcpy(mem, s);
        }

        return mem;
}


void test_basics()
{
        TEST_NAME();

        llist_t *list = llist_new(NULL, NULL);
        ASSERT(list, "NULL object");
        ASSERT(llist_size(list) == 0, "List size != 0");
        ASSERT(llist_empty(list) == true, "List is not empty");
        ASSERT(llist_delete(list), "List delete error");
        ASSERT(llist_size(list) == -1, "List use invalid object");

        TEST_SUCCESS();
}

void test_push_emplace_front()
{
        TEST_NAME();

        llist_t *list = llist_new(NULL, NULL);

        for (int i = 0; i < array_count(cs); i++) {
                ASSERT(llist_push_emplace_front(list, strlen(cs[i]) + 1, cs[i]), "Object not created");
        }

        int j = array_count(cs) - 1;
        llist_foreach(char *,str, list) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[j]) == 0, "Value is not equal to expected one (foreach)");
                j--;
        }

        for (int i = 0; i < llist_size(list); i++) {
                void *str = llist_at(list, i);
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[array_count(cs) - i - 1]) == 0, "Value is not equal to expected one (at)");
        }


        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_push_front()
{
        TEST_NAME();

        llist_t *list = llist_new(NULL, NULL);

        for (int i = 0; i < array_count(cs); i++) {
                ds[i] = string_new(cs[i]);
                ASSERT(ds[i], "Test error: NULL object created");
                ASSERT(llist_push_front(list, ds[i]), "Object not added");
        }

        int j = array_count(ds) - 1;
        llist_foreach(char *,str, list) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, ds[j]) == 0, "Value is not equal to expected one (foreach)");
                j--;
        }


        for (int i = 0; i < llist_size(list); i++) {
                void *str = llist_at(list, i);
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, ds[array_count(ds) - i - 1]) == 0, "Value is not equal to expected one (at)");
        }


        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_pop_front()
{
        TEST_NAME();

        llist_t *list = llist_new(NULL, NULL);

        for (int i = 0; i < array_count(cs); i++) {
                ds[i] = string_new(cs[i]);
                ASSERT(ds[i], "Test error: NULL object created");
                ASSERT(llist_push_front(list, ds[i]), "Object not added");
        }

        ASSERT(llist_pop_front(list), "First element is not poped");
        ASSERT(llist_pop_front(list), "First element is not poped");


        int j = array_count(ds) - 3;
        llist_foreach(char *,str, list) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, ds[j]) == 0, "Value is not equal to expected one (foreach)");
                j--;
        }

        for (int i = 0; i < llist_size(list); i++) {
                void *str = llist_at(list, i);
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, ds[array_count(ds) - i - 3]) == 0, "Value is not equal to expected one (at)");
        }


        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_push_emplace_back()
{
        TEST_NAME();

        llist_t *list = llist_new(NULL, NULL);

        for (int i = 0; i < array_count(cs); i++) {
                ASSERT(llist_push_emplace_back(list, strlen(cs[i]) + 1, cs[i]), "Object not created");
        }

        int j = 0;
        llist_foreach(char *,str, list) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[j]) == 0, "Value is not equal to expected one (foreach)");
                j++;
        }


        for (int i = 0; i < llist_size(list); i++) {
                void *str = llist_at(list, i);
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[i]) == 0, "Value is not equal to expected one (at)");
        }


        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_push_back()
{
        TEST_NAME();

        llist_t *list = llist_new(NULL, NULL);

        for (int i = 0; i < array_count(cs); i++) {
                ds[i] = string_new(cs[i]);
                ASSERT(ds[i], "Test error: NULL object created");
                ASSERT(llist_push_back(list, ds[i]), "Object not added");
        }

        int j = 0;
        llist_foreach(char *,str, list) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, ds[j]) == 0, "Value is not equal to expected one (foreach)");
                j++;
        }


        for (int i = 0; i < llist_size(list); i++) {
                void *str = llist_at(list, i);
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, ds[i]) == 0, "Value is not equal to expected one (at)");
        }


        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_pop_back()
{
        TEST_NAME();

        llist_t *list = llist_new(NULL, NULL);

        for (int i = 0; i < array_count(cs); i++) {
                ds[i] = string_new(cs[i]);
                ASSERT(ds[i], "Test error: NULL object created");
                ASSERT(llist_push_back(list, ds[i]), "Object not added");
        }

        ASSERT(llist_pop_back(list), "First element is not poped");
        ASSERT(llist_pop_back(list), "First element is not poped");


        int j = 0;
        llist_foreach(char *,str, list) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, ds[j]) == 0, "Value is not equal to expected one (foreach)");
                j++;
        }


        for (int i = 0; i < llist_size(list); i++) {
                void *str = llist_at(list, i);
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, ds[i]) == 0, "Value is not equal to expected one (at)");
        }


        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_emplace()
{
        TEST_NAME();

        llist_t *list = llist_new(NULL, NULL);

        //----------------------------------------------------------------------
        // adds to next indexes 0..9
        //----------------------------------------------------------------------
        for (int i = 0; i < array_count(cs); i++) {
                ASSERT(llist_emplace(list, i, strlen(cs[i]) + 1, cs[i]), "Object not created");
        }

        int j = 0;
        llist_foreach(char *,str, list) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[j]) == 0, "Value is not equal to expected one (foreach)");
                j++;
        }


        for (int i = 0; i < llist_size(list); i++) {
                void *str = llist_at(list, i);
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[i]) == 0, "Value is not equal to expected one (at)");
        }


        //----------------------------------------------------------------------
        // adds always to index 0
        //----------------------------------------------------------------------
        ASSERT(llist_clear(list), "List clear error");
        for (int i = 0; i < array_count(cs); i++) {
                ASSERT(llist_emplace(list, 0, strlen(cs[i]) + 1, cs[i]), "Object not created");
        }


        j = array_count(cs) - 1;
        llist_foreach(char *,str, list) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[j]) == 0, "Value is not equal to expected one (foreach)");
                j--;
        }


        for (int i = 0; i < llist_size(list); i++) {
                void *str = llist_at(list, i);
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[array_count(cs) - i - 1]) == 0, "Value is not equal to expected one (at)");
        }


        //----------------------------------------------------------------------
        // adds to next indexes 0..9 and at the end adds at position 3 string 5
        //----------------------------------------------------------------------
        ASSERT(llist_clear(list), "List clear error");
        for (int i = 0; i < array_count(cs); i++) {
                ASSERT(llist_emplace(list, i, strlen(cs[i]) + 1, cs[i]), "Object not created");
        }

        ASSERT(llist_emplace(list, 3, strlen(cs[5]) + 1, cs[5]), "Object not created");
        ASSERT(llist_size(list) == array_count(cs) + 1, "List size is invalid");

        for (int i = 0; i < llist_size(list); i++) {
                void *str = llist_at(list, i);
                ASSERT(str, "NULL string %d", i);
        }

        ASSERT(strcmp(llist_at(list,  0), cs[0]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list,  1), cs[1]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list,  2), cs[2]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list,  3), cs[5]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list,  4), cs[3]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list,  5), cs[4]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list,  6), cs[5]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list,  7), cs[6]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list,  8), cs[7]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list,  9), cs[8]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 10), cs[9]) == 0, "String not equal");


        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_insert()
{
        TEST_NAME();

        llist_t *list = llist_new(NULL, NULL);

        //----------------------------------------------------------------------
        // adds to next indexes 0..9
        //----------------------------------------------------------------------
        for (int i = 0; i < array_count(cs); i++) {
                ASSERT(llist_insert(list, i, string_new(cs[i])), "Object not created");
        }


        int j = 0;
        llist_foreach(char *,str, list) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[j]) == 0, "Value is not equal to expected one (foreach)");
                j++;
        }


        for (int i = 0; i < llist_size(list); i++) {
                void *str = llist_at(list, i);
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[i]) == 0, "Value is not equal to expected one (at)");
        }


        //----------------------------------------------------------------------
        // adds always to index 0
        //----------------------------------------------------------------------
        ASSERT(llist_clear(list), "List clear error");
        for (int i = 0; i < array_count(cs); i++) {
                ASSERT(llist_insert(list, 0, string_new(cs[i])), "Object not created");
        }

        j = array_count(cs) - 1;
        llist_foreach(char *,str, list) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[j]) == 0, "Value is not equal to expected one (foreach)");
                j--;
        }


        for (int i = 0; i < llist_size(list); i++) {
                void *str = llist_at(list, i);
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[array_count(cs) - i - 1]) == 0, "Value is not equal to expected one (at)");
        }


        //----------------------------------------------------------------------
        // adds to next indexes 0..9 and at the end adds at position 3 string 5
        //----------------------------------------------------------------------
        ASSERT(llist_clear(list), "List clear error");
        for (int i = 0; i < array_count(cs); i++) {
                ASSERT(llist_insert(list, i, string_new(cs[i])), "Object not created");
        }

        ASSERT(llist_insert(list, 3, string_new(cs[5])), "Object not created");
        ASSERT(llist_size(list) == array_count(cs) + 1, "List size is invalid");

        for (int i = 0; i < llist_size(list); i++) {
                void *str = llist_at(list, i);
                ASSERT(str, "NULL string %d", i);
        }

        ASSERT(strcmp(llist_at(list,  0), cs[0]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list,  1), cs[1]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list,  2), cs[2]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list,  3), cs[5]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list,  4), cs[3]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list,  5), cs[4]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list,  6), cs[5]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list,  7), cs[6]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list,  8), cs[7]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list,  9), cs[8]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 10), cs[9]) == 0, "String not equal");


        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_erase()
{
        TEST_NAME();

        llist_t *list = llist_new(NULL, NULL);


        for (int i = 0; i < array_count(cs); i++) {
                ASSERT(llist_insert(list, i, string_new(cs[i])), "Object not created");
        }


        int j = 0;
        llist_foreach(char *,str, list) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[j]) == 0, "Value is not equal to expected one (foreach)");
                j++;
        }


        for (int i = 0; i < llist_size(list); i++) {
                void *str = llist_at(list, i);
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[i]) == 0, "Value is not equal to expected one (at)");
        }

        ASSERT(llist_erase(list, 5), "Element erase error");


        ASSERT(strcmp(llist_at(list, 0), cs[ 0]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 1), cs[ 1]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 2), cs[ 2]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 3), cs[ 3]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 4), cs[ 4]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 5), cs[ 6]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 6), cs[ 7]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 7), cs[ 8]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 8), cs[ 9]) == 0, "String not equal");


        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_take()
{
        TEST_NAME();

        llist_t *list = llist_new(NULL, NULL);


        for (int i = 0; i < array_count(cs); i++) {
                ds[i] = string_new(cs[i]);
                ASSERT(llist_insert(list, i, ds[i]), "Object not created");
        }


        int j = 0;
        llist_foreach(char *,str, list) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[j]) == 0, "Value is not equal to expected one (foreach)");
                j++;
        }


        for (int i = 0; i < llist_size(list); i++) {
                void *str = llist_at(list, i);
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[i]) == 0, "Value is not equal to expected one (at)");
        }

        char *s = llist_take(list, 5);
        ASSERT(s == ds[5], "Element erase error");

        s = llist_take(list, 7);
        ASSERT(s == ds[8], "Element erase error");

        ASSERT(strcmp(llist_at(list, 0), cs[ 0]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 1), cs[ 1]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 2), cs[ 2]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 3), cs[ 3]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 4), cs[ 4]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 5), cs[ 6]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 6), cs[ 7]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 7), cs[ 9]) == 0, "String not equal");


        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_take_front()
{
        TEST_NAME();

        llist_t *list = llist_new(NULL, NULL);


        for (int i = 0; i < array_count(cs); i++) {
                ds[i] = string_new(cs[i]);
                ASSERT(llist_insert(list, i, ds[i]), "Object not created");
        }


        int j = 0;
        llist_foreach(char *,str, list) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[j]) == 0, "Value is not equal to expected one (foreach)");
                j++;
        }


        for (int i = 0; i < llist_size(list); i++) {
                void *str = llist_at(list, i);
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[i]) == 0, "Value is not equal to expected one (at)");
        }

        char *s = llist_take_front(list);
        ASSERT(s == ds[0], "Element erase error");

        s = llist_take_front(list);
        ASSERT(s == ds[1], "Element erase error");

        ASSERT(strcmp(llist_at(list, 0), cs[2]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 1), cs[3]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 2), cs[4]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 3), cs[5]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 4), cs[6]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 5), cs[7]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 6), cs[8]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 7), cs[9]) == 0, "String not equal");


        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_take_back()
{
        TEST_NAME();

        llist_t *list = llist_new(NULL, NULL);


        for (int i = 0; i < array_count(cs); i++) {
                ds[i] = string_new(cs[i]);
                ASSERT(llist_insert(list, i, ds[i]), "Object not created");
        }


        int j = 0;
        llist_foreach(char *,str, list) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[j]) == 0, "Value is not equal to expected one (foreach)");
                j++;
        }


        for (int i = 0; i < llist_size(list); i++) {
                void *str = llist_at(list, i);
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[i]) == 0, "Value is not equal to expected one (at)");
        }

        char *s = llist_take_back(list);
        ASSERT(s == ds[9], "Element erase error");

        s = llist_take_back(list);
        ASSERT(s == ds[8], "Element erase error");

        ASSERT(strcmp(llist_at(list, 0), cs[0]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 1), cs[1]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 2), cs[2]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 3), cs[3]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 4), cs[4]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 5), cs[5]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 6), cs[6]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 7), cs[7]) == 0, "String not equal");


        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_clear()
{
        TEST_NAME();

        llist_t *list = llist_new(NULL, NULL);


        for (int i = 0; i < array_count(cs); i++) {
                ds[i] = string_new(cs[i]);
                ASSERT(llist_insert(list, i, ds[i]), "Object not created");
        }


        int j = 0;
        llist_foreach(char *,str, list) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[j]) == 0, "Value is not equal to expected one (foreach)");
                j++;
        }


        for (int i = 0; i < llist_size(list); i++) {
                void *str = llist_at(list, i);
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[i]) == 0, "Value is not equal to expected one (at)");
        }

        ASSERT(llist_empty(list) == false, "List is empty");
        ASSERT(llist_size(list) == 10, "List not contains elements");
        ASSERT(llist_clear(list), "List clear error");
        ASSERT(llist_size(list) == 0, "List contains elements");
        ASSERT(llist_empty(list), "List is not empty");

        llist_foreach(char*, str, list) {
                ASSERT(false, "List is empty and foreach works!");
        }

        llist_foreach_reverse(char*, str, list) {
                ASSERT(false, "List is empty and foreach works!");
        }

        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_sort()
{
        TEST_NAME();

        llist_t *list = llist_new((llist_cmp_functor_t)strcmp, NULL);

        ASSERT(llist_push_emplace_back(list, strlen(cs[0]) + 1, cs[0]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[2]) + 1, cs[2]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[4]) + 1, cs[4]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[6]) + 1, cs[6]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[8]) + 1, cs[8]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[1]) + 1, cs[1]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[3]) + 1, cs[3]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[5]) + 1, cs[5]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[7]) + 1, cs[7]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[9]) + 1, cs[9]), "Object is not created");

        ASSERT(strcmp(llist_at(list, 0), cs[0]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 1), cs[2]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 2), cs[4]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 3), cs[6]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 4), cs[8]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 5), cs[1]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 6), cs[3]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 7), cs[5]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 8), cs[7]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 9), cs[9]) == 0, "Objects are not equal");

        llist_sort(list);

        ASSERT(strcmp(llist_at(list, 0), cs[0]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 1), cs[1]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 2), cs[2]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 3), cs[3]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 4), cs[4]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 5), cs[5]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 6), cs[6]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 7), cs[7]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 8), cs[8]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 9), cs[9]) == 0, "Objects are not equal");

        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_unique()
{
        TEST_NAME();

        llist_t *list = llist_new((llist_cmp_functor_t)strcmp, NULL);

        ASSERT(llist_push_emplace_back(list, strlen(cs[0]) + 1, cs[0]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[2]) + 1, cs[2]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[2]) + 1, cs[2]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[4]) + 1, cs[4]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[6]) + 1, cs[6]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[8]) + 1, cs[8]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[8]) + 1, cs[8]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[8]) + 1, cs[8]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[8]) + 1, cs[8]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[1]) + 1, cs[1]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[3]) + 1, cs[3]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[5]) + 1, cs[5]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[7]) + 1, cs[7]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[9]) + 1, cs[9]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[8]) + 1, cs[8]), "Object is not created");

        ASSERT(strcmp(llist_at(list,  0), cs[0]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list,  1), cs[2]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list,  2), cs[2]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list,  3), cs[4]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list,  4), cs[6]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list,  5), cs[8]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list,  6), cs[8]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list,  7), cs[8]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list,  8), cs[8]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list,  9), cs[1]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 10), cs[3]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 11), cs[5]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 12), cs[7]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 13), cs[9]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 14), cs[8]) == 0, "Objects are not equal");

        llist_unique(list);

        ASSERT(strcmp(llist_at(list, 0), cs[0]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 1), cs[1]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 2), cs[2]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 3), cs[3]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 4), cs[4]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 5), cs[5]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 6), cs[6]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 7), cs[7]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 8), cs[8]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 9), cs[9]) == 0, "Objects are not equal");

        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_reverse()
{
        TEST_NAME();

        llist_t *list = llist_new(NULL, NULL);

        ASSERT(llist_push_emplace_back(list, strlen(cs[0]) + 1, cs[0]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[1]) + 1, cs[1]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[2]) + 1, cs[2]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[3]) + 1, cs[3]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[4]) + 1, cs[4]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[5]) + 1, cs[5]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[6]) + 1, cs[6]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[7]) + 1, cs[7]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[8]) + 1, cs[8]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[9]) + 1, cs[9]), "Object is not created");

        ASSERT(strcmp(llist_at(list, 0), cs[0]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 1), cs[1]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 2), cs[2]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 3), cs[3]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 4), cs[4]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 5), cs[5]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 6), cs[6]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 7), cs[7]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 8), cs[8]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 9), cs[9]) == 0, "Objects are not equal");

        llist_reverse(list);

        ASSERT(strcmp(llist_at(list, 0), cs[9]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 1), cs[8]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 2), cs[7]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 3), cs[6]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 4), cs[5]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 5), cs[4]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 6), cs[3]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 7), cs[2]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 8), cs[1]) == 0, "Objects are not equal");
        ASSERT(strcmp(llist_at(list, 9), cs[0]) == 0, "Objects are not equal");

        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_contains()
{
        TEST_NAME();

        llist_t *list = llist_new((llist_cmp_functor_t)strcmp, NULL);

        ASSERT(llist_contains(list, cs[4]) == 0, "Empty list and object is found in it");

        ASSERT(llist_push_emplace_back(list, strlen(cs[0]) + 1, cs[0]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[2]) + 1, cs[2]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[4]) + 1, cs[4]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[6]) + 1, cs[6]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[8]) + 1, cs[8]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[1]) + 1, cs[1]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[3]) + 1, cs[3]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[5]) + 1, cs[5]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[7]) + 1, cs[7]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[9]) + 1, cs[9]), "Object is not created");
        ASSERT(llist_push_emplace_back(list, strlen(cs[9]) + 1, cs[9]), "Object is not created");

        ASSERT(llist_contains(list, cs[4]) == 1, "List not contains selected object");
        ASSERT(llist_contains(list, cs[9]) == 2, "List not contains selected object");

        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_find_begin()
{
        TEST_NAME();

        llist_t *list = llist_new((llist_cmp_functor_t)strcmp, NULL);

        ASSERT(llist_contains(list, cs[4]) == 0, "Empty list and object is found in it");

        for (int i = 0; i < array_count(cs); i++) {
                ds[i] = string_new(cs[i]);
        }

        ASSERT(llist_push_back(list, ds[0]), "Object is not created");
        ASSERT(llist_push_back(list, ds[2]), "Object is not created");
        ASSERT(llist_push_back(list, ds[4]), "Object is not created");
        ASSERT(llist_push_back(list, ds[6]), "Object is not created");
        ASSERT(llist_push_back(list, ds[8]), "Object is not created");
        ASSERT(llist_push_back(list, ds[1]), "Object is not created");
        ASSERT(llist_push_back(list, ds[3]), "Object is not created");
        ASSERT(llist_push_back(list, ds[5]), "Object is not created");
        ASSERT(llist_push_back(list, ds[7]), "Object is not created");

        ASSERT(llist_find_begin(list, cs[4]) ==  2, "List not contains selected object");
        ASSERT(llist_find_begin(list, cs[9]) == -1, "List contains not added object");

        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_find_end()
{
        TEST_NAME();

        llist_t *list = llist_new((llist_cmp_functor_t)strcmp, NULL);

        ASSERT(llist_contains(list, cs[4]) == 0, "Empty list and object is found in it");

        for (int i = 0; i < array_count(cs); i++) {
                ds[i] = string_new(cs[i]);
        }

        ASSERT(llist_push_back(list, ds[0]), "Object is not created");
        ASSERT(llist_push_back(list, ds[2]), "Object is not created");
        ASSERT(llist_push_back(list, ds[4]), "Object is not created");
        ASSERT(llist_push_back(list, ds[6]), "Object is not created");
        ASSERT(llist_push_back(list, ds[8]), "Object is not created");
        ASSERT(llist_push_back(list, ds[1]), "Object is not created");
        ASSERT(llist_push_back(list, ds[3]), "Object is not created");
        ASSERT(llist_push_back(list, ds[5]), "Object is not created");
        ASSERT(llist_push_back(list, ds[7]), "Object is not created");

        ASSERT(llist_find_end(list, cs[8]) ==  4, "List not contains selected object");
        ASSERT(llist_find_end(list, cs[9]) == -1, "List contains not added object");

        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_front_back()
{
        TEST_NAME();

        llist_t *list = llist_new(NULL, NULL);

        for (int i = 0; i < array_count(cs); i++) {
                ds[i] = string_new(cs[i]);
        }

        ASSERT(llist_push_back(list, ds[0]), "Object is not created");
        ASSERT(llist_push_back(list, ds[2]), "Object is not created");
        ASSERT(llist_push_back(list, ds[4]), "Object is not created");
        ASSERT(llist_push_back(list, ds[6]), "Object is not created");
        ASSERT(llist_push_back(list, ds[8]), "Object is not created");
        ASSERT(llist_push_back(list, ds[1]), "Object is not created");
        ASSERT(llist_push_back(list, ds[3]), "Object is not created");
        ASSERT(llist_push_back(list, ds[5]), "Object is not created");
        ASSERT(llist_push_back(list, ds[7]), "Object is not created");
        ASSERT(llist_push_back(list, ds[9]), "Object is not created");

        ASSERT(llist_front(list) == ds[0], "First element is not as expected");
        ASSERT(llist_back(list) == ds[9], "The last element is not as expected");

        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_swap()
{
        TEST_NAME();

        llist_t *list = llist_new(NULL, NULL);

        for (int i = 0; i < array_count(cs); i++) {
                ds[i] = string_new(cs[i]);
        }

        ASSERT(llist_push_back(list, ds[0]), "Object is not created");
        ASSERT(llist_push_back(list, ds[1]), "Object is not created");
        ASSERT(llist_push_back(list, ds[2]), "Object is not created");
        ASSERT(llist_push_back(list, ds[3]), "Object is not created");
        ASSERT(llist_push_back(list, ds[4]), "Object is not created");
        ASSERT(llist_push_back(list, ds[5]), "Object is not created");
        ASSERT(llist_push_back(list, ds[6]), "Object is not created");
        ASSERT(llist_push_back(list, ds[7]), "Object is not created");
        ASSERT(llist_push_back(list, ds[8]), "Object is not created");
        ASSERT(llist_push_back(list, ds[9]), "Object is not created");

        ASSERT(llist_swap(list, 3, 7), "Swap error");

        ASSERT(strcmp(llist_at(list, 0), ds[0]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 1), ds[1]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 2), ds[2]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 3), ds[7]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 4), ds[4]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 5), ds[5]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 6), ds[6]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 7), ds[3]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 8), ds[8]) == 0, "String not equal");
        ASSERT(strcmp(llist_at(list, 9), ds[9]) == 0, "String not equal");

        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_object_destructor()
{
        struct obj {
                int a;
                int b;
        };

        void destructor(struct obj *this)
        {
                ASSERT(this, "Null object");

                if (this) {
                        this->a = 0;
                        this->b = 0;
                }
        }

        TEST_NAME();

        llist_t *list = llist_new(NULL, (llist_obj_dtor_t)destructor);

        struct obj *o = malloc(sizeof(struct obj));
        ASSERT(o, "Object not allocated");
        o->a = 100;
        o->b = 50;

        ASSERT(llist_push_back(list, o), "Object not pushed to the list");
        ASSERT(llist_erase(list, 0), "Object not erased");
        ASSERT(o->a == 0, "Object not destroyed");
        ASSERT(o->b == 0, "Object not destroyed");

        free(o);

        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

void test_iterator()
{
        TEST_NAME();

        llist_t *list = llist_new(NULL, NULL);

        for (int i = 0; i < array_count(cs); i++) {
                ds[i] = string_new(cs[i]);
                ASSERT(ds[i], "Test error: NULL object created");
                ASSERT(llist_push_back(list, ds[i]), "Object not added");
        }


        int l = 0;
        int k = 0;
        llist_foreach(char *,str, list) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, ds[k]) == 0, "Value is not equal to expected one (foreach)");
                k++;
        }


        k = 0;
        llist_iterator_t li = llist_iterator(list);
        for (char *str = llist_begin(&li); str; str = llist_iterator_next(&li)) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, ds[k++]) == 0, "Value is not equal to expected one (iterator)");
        }


        k  = array_count(ds);
        li = llist_iterator(list);
        for (char *str = llist_end(&li); str; str = llist_iterator_prev(&li)) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, ds[--k]) == 0, "Value is not equal to expected one (iterator)");
        }


        k = 5; l = 0;
        for (char *str = llist_range(&li, 5, 7); str; str = llist_iterator_next(&li), k++, l++) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, ds[k]) == 0, "Value is not equal to expected one (iterator range)");
                ASSERT(k >= 5 && k <= 7, "Out of iterator range");
        }
        ASSERT(l == 3, "Iterator not encounter to bound");


        k = 5; l = 0;
        for (char *str = llist_range(&li, 5, 5); str; str = llist_iterator_next(&li), k++, l++) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, ds[k]) == 0, "Value is not equal to expected one (iterator range)");
                ASSERT(k == 5, "Out of iterator range");
        }
        ASSERT(l == 1, "Iterator not encounter to bound");


        k = 0; l = 0;
        for (char *str = llist_range(&li, 0, 0); str; str = llist_iterator_next(&li), k++, l++) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, ds[k]) == 0, "Value is not equal to expected one (iterator range)");
                ASSERT(k == 0, "Out of iterator range");
        }
        ASSERT(l == 1, "Iterator not encounter to bound");


        k = 7; l = 0;
        for (char *str = llist_range(&li, 7, 5); str; str = llist_iterator_prev(&li), k--, l++) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, ds[k]) == 0, "Value is not equal to expected one (iterator range)");
                ASSERT(k >= 5 && k <= 7, "Out of iterator range");
        }
        ASSERT(l == 3, "Iterator not encounter to bound");


        k = 8; l = 0;
        for (char *str = llist_range(&li, 8, 8); str; str = llist_iterator_prev(&li), k--, l++) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, ds[k]) == 0, "Value is not equal to expected one (iterator range)");
                ASSERT(k == 8, "Out of iterator range");
        }
        ASSERT(l == 1, "Iterator not encounter to bound");


        // test item remove by using iterator (foreach)
        int n = 0;
        llist_foreach(char *,str, list) {
                ASSERT(str, "NULL string");

                if (n >= 5) {
                        llist_erase_by_iterator(&llist_foreach_iterator);
                }

                n++;

        }

        ASSERT(llist_size(list) == 5, "Elements not erased");

        n = 0;
        llist_foreach(char *,str, list) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[n]) == 0, "String is not as expected");
                n++;
        }


        // test item remove by using iterator (foreach reverse)
        ASSERT(llist_clear(list) == 1, "List erase error");

        for (int i = 0; i < array_count(cs); i++) {
                ds[i] = string_new(cs[i]);
                ASSERT(ds[i], "Test error: NULL object created");
                ASSERT(llist_push_back(list, ds[i]), "Object not added");
        }

        n = array_count(cs) - 1;
        llist_foreach_reverse(char *,str, list) {
                ASSERT(str, "NULL string");

                if (n >= 5) {
                        llist_erase_by_iterator(&llist_foreach_iterator);
                }

                n--;

        }

        ASSERT(llist_size(list) == 5, "Elements not erased");

        n = 4;
        llist_foreach_reverse(char *,str, list) {
                ASSERT(str, "NULL string");
                ASSERT(strcmp(str, cs[n]) == 0, "String is not as expected");
                n--;
        }


        // 1 item erase by using iterator
        ASSERT(llist_clear(list) == 1, "List erase error");
        ds[0] = string_new(cs[0]);
        ASSERT(ds[0], "Test error: NULL object created");
        ASSERT(llist_push_back(list, ds[0]), "Object not added");

        ASSERT(llist_size(list) == 1, "Elements not added");

        llist_foreach(char *, str, list) {
                ASSERT(str, "NULL string");
                llist_erase_by_iterator(&llist_foreach_iterator);
        }

        ASSERT(llist_size(list) == 0, "Elements not erased");



        ASSERT(llist_delete(list), "List delete error");

        TEST_SUCCESS();
}

int main(void)
{
        test_basics();
        test_push_emplace_front();
        test_push_front();
        test_pop_front();
        test_push_emplace_back();
        test_push_back();
        test_pop_back();
        test_emplace();
        test_insert();
        test_erase();
        test_take();
        test_take_front();
        test_take_back();
        test_clear();
        test_sort();
        test_unique();
        test_reverse();
        test_contains();
        test_find_begin();
        test_find_end();
        test_front_back();
        test_swap();
        test_object_destructor();
        test_iterator();

        return 0;
}
